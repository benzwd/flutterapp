import 'package:flutter/material.dart';
import 'package:flutter_profile_picture/flutter_profile_picture.dart';
import 'package:carousel_slider/carousel_slider.dart';

import 'assets/drawer.dart';
import 'assets/participant.dart';
import 'assets/team.dart';
import 'assets/utils.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          drawerTheme: DrawerThemeData(scrimColor: Colors.transparent)),
      title: 'Sort app',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey();
  List<Participant> participants = [];
  List<String> teams = [];
  List<Equipe> teamsGenerate = [];
  int _currentTeamIndex = 0;
  bool isGenerate = false;

  String selectedMenu = "Normal";

  void updateMenu(String menu) {
    setState(() {
      selectedMenu = menu;
    });
  }

  void addParticipant(String name, {String? gender = 'Homme', int? level = 1}) {
    setState(() {
      participants.add(Participant(name: name, gender: gender, level: level));
    });
  }

  void refreshApp() {
    setState(() {
      participants.clear();
      teams.clear();
      teamsGenerate.clear();
      isGenerate = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      backgroundColor: Color(0xFF171717),
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 40, left: 5, right: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                        onPressed: () {
                          _globalKey.currentState!.openDrawer();
                        },
                        icon: Icon(
                          Icons.menu,
                          color: Colors.white,
                        )),
                    IconButton(
                        onPressed: refreshApp,
                        icon: Icon(
                          Icons.refresh,
                          color: Colors.white,
                        )),
                  ],
                ),
              ),
              SizedBox(
                height: 40,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.only(left: 10),
                  children: [
                    TextButton(
                        onPressed: () {
                          updateMenu('Normal');
                        },
                        child: Text(
                          "Normal",
                          style: TextStyle(
                              color: selectedMenu == "Normal"
                                  ? Colors.white
                                  : Colors.grey,
                              fontSize: 22),
                        )),
                    SizedBox(
                      width: 25,
                    ),
                    TextButton(
                        onPressed: () {
                          updateMenu('Genre');
                        },
                        child: Text(
                          "Genre",
                          style: TextStyle(
                              color: selectedMenu == "Genre"
                                  ? Colors.white
                                  : Colors.grey,
                              fontSize: 22),
                        )),
                    SizedBox(
                      width: 25,
                    ),
                    TextButton(
                        onPressed: () {
                          updateMenu('Niveau');
                        },
                        child: Text(
                          "Niveau",
                          style: TextStyle(
                              color: selectedMenu == "Niveau"
                                  ? Colors.white
                                  : Colors.grey,
                              fontSize: 22),
                        )),
                    SizedBox(
                      width: 25,
                    ),
                    TextButton(
                        onPressed: () {},
                        child: Text(
                          "Mes tirages",
                          style: TextStyle(color: Colors.grey, fontSize: 22),
                        )),
                    SizedBox(
                      width: 25,
                    )
                  ],
                ),
              ),
            ],
          ),
          Positioned(
              top: 150,
              left: 0,
              right: 0,
              child: Container(
                padding: EdgeInsets.only(top: 15, left: 25, right: 25),
                height: 240,
                decoration: BoxDecoration(
                  color: Colors.orange,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40)),
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Participants (${participants.length})",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w500),
                        ),
                        IconButton(
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return ParticipantDialog(
                                    selectedMenu: selectedMenu,
                                    onParticipantAdded: addParticipant,
                                  );
                                },
                              );
                            },
                            icon: Icon(
                              Icons.person_add,
                              color: Colors.white,
                            ))
                      ],
                    ),
                    SizedBox(
                      height: 120,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: participants.length,
                        itemBuilder: (BuildContext context, int index) {
                          return buildContactAvatar(participants[index].name,
                              level: participants[index].level,
                              gender: participants[index].gender);
                        },
                      ),
                    )
                  ],
                ),
              )),
          Positioned(
              top: 330,
              left: 0,
              right: 0,
              bottom: 0,
              child: Container(
                padding: EdgeInsets.only(top: 15, left: 25, right: 25),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40)),
                    color: Color(0xFFEFFFFC)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Équipes (${teams.length})",
                          style: TextStyle(
                              color: Colors.orange,
                              fontSize: 18,
                              fontWeight: FontWeight.w500),
                        ),
                        IconButton(
                            onPressed: () {
                              setState(() {
                                int newTeamNumber = teams.length + 1;
                                teams.add('Team $newTeamNumber');
                              });
                            },
                            icon: Icon(
                              Icons.add,
                              color: Colors.orange,
                            )),
                      ],
                    ),
                    Text(
                      _currentTeamIndex < teams.length
                          ? teams[_currentTeamIndex]
                          : '',
                      style: TextStyle(
                        color: Colors.orange,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Expanded(child: _buildTeamSlide(teamsGenerate, teams)),
                  ],
                ),
              )),
        ],
      ),
      drawer: Drawer(
        width: 275,
        backgroundColor: Colors.black26,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.horizontal(right: Radius.circular(40))),
        child: Container(
          decoration: BoxDecoration(
              color: Color(0xF71F1E1E),
              borderRadius: BorderRadius.horizontal(right: Radius.circular(40)),
              boxShadow: [
                BoxShadow(
                    color: Color(0x3D000000), spreadRadius: 30, blurRadius: 20)
              ]),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 50, 20, 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Row(
                      children: [
                        IconButton(
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                            size: 20,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Text(
                          "Menu",
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 35,
                    ),
                    DrawerItem(
                      title: 'Mes tirages',
                      icon: Icons.archive,
                    ),
                    Divider(
                      height: 35,
                      color: Colors.orange,
                    ),
                    DrawerItem(
                      title: 'Aide',
                      icon: Icons.help,
                    ),
                  ],
                ),
                DrawerItem(
                    title: 'Inviter des amis', icon: Icons.people_outline)
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            if (isGenerate) {
              isGenerate = !isGenerate;
              showAlert(context, "La fonction n'est pas encore disponible.");
            } else {
              _currentTeamIndex = 0;
              teamsGenerate = TeamGenerator.generateTeams(
                  participants, teams, selectedMenu, context);
              if (participants.length.toInt() > 0) isGenerate = true;
            }
          });
        },
        enableFeedback: false,
        backgroundColor: Color(0xFF171717),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
        child: isGenerate
            ? Icon(
                Icons.save,
                color: Colors.white,
              )
            : Icon(
                Icons.all_inclusive,
                color: Colors.white,
              ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }

  Padding buildContactAvatar(String name, {String? gender, int? level}) {
    return Padding(
      padding: EdgeInsets.only(right: 20),
      child: Column(
        children: [
          ProfilePicture(
            name: name,
            radius: 31,
            fontsize: 21,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            name,
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
          if (selectedMenu == 'Niveau')
            Text(
              "Niveau : ${level.toString()}",
              style: TextStyle(color: Colors.white, fontSize: 12),
            ),
          if (selectedMenu == 'Genre')
            Text(
              gender.toString(),
              style: TextStyle(color: Colors.white, fontSize: 12),
            ),
        ],
      ),
    );
  }

  Widget _buildTeamSlide(
    List<Equipe> equipes,
    List<String> teamNames,
  ) {
    return teamNames.isNotEmpty
        ? CarouselSlider.builder(
            itemCount: teamNames.length,
            itemBuilder: (BuildContext context, int index, int realIndex) {
              if (equipes.isNotEmpty && index < equipes.length) {
                Equipe equipe = equipes[index];
                return Container(
                  padding: EdgeInsets.only(left: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.orange,
                  ),
                  child: ListView(
                    scrollDirection: Axis.vertical,
                    children: [
                      Wrap(
                        spacing: 1.0,
                        runSpacing: 20.0,
                        alignment: WrapAlignment.spaceAround,
                        children: equipe.participants.map((participant) {
                          return buildContactAvatar(
                            participant.name,
                            level: participant.level,
                            gender: participant.gender,
                          );
                        }).toList(),
                      )
                    ],
                  ),
                );
              } else {
                return SizedBox(); // Retourne un widget vide si la liste d'équipes est vide
              }
            },
            options: CarouselOptions(
              height: 300,
              viewportFraction: 1.0,
              enableInfiniteScroll: false,
              onPageChanged: (index, _) {
                setState(() {
                  _currentTeamIndex = index;
                });
              },
              autoPlay: false,
            ),
          )
        : Center(
            child: Text("Aucune équipe ajoutée."),
          );
  }
}
