import 'package:flutter/material.dart';

class Participant {
  final String name;
  final String? gender;
  final int? level;

  Participant({required this.name, this.gender, this.level});
}

class ParticipantDialog extends StatefulWidget {
  final String selectedMenu;
  final Function(String name, {String? gender, int? level}) onParticipantAdded;

  const ParticipantDialog(
      {super.key,
      required this.selectedMenu,
      required this.onParticipantAdded});

  @override
  State<ParticipantDialog> createState() => _ParticipantDialogState();
}

class _ParticipantDialogState extends State<ParticipantDialog> {
  late String selectedMenu;
  late TextEditingController _nameController;
  @override
  void initState() {
    super.initState();
    selectedMenu = widget.selectedMenu;
    _nameController = TextEditingController();
  }

  List<bool> _isSelectedGender = [false, false];
  List<int> _selectedLevel = [1, 2, 3, 4, 5];
  int selectedLevelIndex = 0;
  int selectedGenderIndex = 0;

  Widget additionalField = SizedBox();

  void addParticipant() {
    String name = _nameController.text;
    int selectedLevel = selectedLevelIndex + 1;
    String selectedGender =
        _isSelectedGender[selectedGenderIndex] ? 'Homme' : 'Femme';

    switch (selectedMenu) {
      case 'Normal':
        addSimpleParticipant(name);
        break;
      case 'Genre':
        addGenderParticipant(name, selectedGender);
        break;
      case 'Niveau':
        addLevelParticipant(name, selectedLevel);
        break;
      default:
        break;
    }
    _nameController.clear();
  }

  void addSimpleParticipant(String name) {
    widget.onParticipantAdded(name);
    Navigator.pop(context);
  }

  void addLevelParticipant(String name, int level) {
    widget.onParticipantAdded(name, level: level);
    Navigator.pop(context);
  }

  void addGenderParticipant(String name, String gender) {
    widget.onParticipantAdded(name, gender: gender);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    switch (selectedMenu) {
      case 'Normal':
        additionalField = SizedBox();
        break;
      case 'Genre':
        additionalField = ToggleButtons(
          isSelected: _isSelectedGender,
          borderRadius: BorderRadius.all(Radius.circular(8)),
          selectedBorderColor: Colors.orange,
          selectedColor: Colors.white,
          fillColor: Colors.orange,
          color: Colors.orange,
          onPressed: (int index) {
            setState(() {
              for (int buttonIndex = 0;
                  buttonIndex < _isSelectedGender.length;
                  buttonIndex++) {
                if (buttonIndex == index) {
                  _isSelectedGender[buttonIndex] = true;
                } else {
                  _isSelectedGender[buttonIndex] = false;
                }
              }
            });
          },
          children: <Widget>[
            Icon(Icons.male),
            Icon(Icons.female),
          ],
        );
        break;
      case 'Niveau':
        additionalField = ToggleButtons(
          isSelected: List.generate(
              _selectedLevel.length, (index) => index == selectedLevelIndex),
          borderRadius: BorderRadius.all(Radius.circular(8)),
          selectedBorderColor: Colors.orange,
          selectedColor: Colors.white,
          fillColor: Colors.orange,
          color: Colors.orange,
          onPressed: (int index) {
            setState(() {
              selectedLevelIndex = index;
            });
          },
          children:
              _selectedLevel.map((level) => Text(level.toString())).toList(),
        );
        break;
      default:
        additionalField = SizedBox();
    }
    return AlertDialog(
      title: Text('Ajouter un participant'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
            controller: _nameController,
            decoration: InputDecoration(hintText: "Prénom"),
          ),
          SizedBox(
            height: 20,
          ),
          additionalField
        ],
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context, 'Cancel'),
          child: Text(
            'Annuler',
            style: TextStyle(color: Colors.grey),
          ),
        ),
        TextButton(
          onPressed: addParticipant,
          child: Text(
            'Ajouter',
            style: TextStyle(color: Colors.orange),
          ),
        ),
      ],
    );
  }
}
