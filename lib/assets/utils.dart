import 'package:flutter/material.dart';

void showAlert(BuildContext context, String message) {
  // Affiche une boîte de dialogue avec le message d'erreur
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text("Erreur"),
        content: Text(message),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text("OK"),
          ),
        ],
      );
    },
  );
}
