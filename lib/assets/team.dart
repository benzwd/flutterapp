import 'package:flutter/material.dart';
import '/assets/participant.dart';
import '/assets/utils.dart';

class Equipe {
  final String nom;
  final List<Participant> participants;

  Equipe({required this.nom, required this.participants});
}

class TeamGenerator {
  static List<Equipe> generateTeams(List<Participant> participants,
      List<String> teamNames, String mode, BuildContext context) {
    List<Equipe> teams = [];
    List<Participant> copyParticipants = List.from(participants);

    if (copyParticipants.isEmpty) {
      showAlert(context, "Aucun participant n'est disponible.");
    } else {
      if (mode == 'Normal') {
        copyParticipants.shuffle();
        int teamSize = (copyParticipants.length / teamNames.length).ceil();
        for (int i = 0; i < copyParticipants.length; i += teamSize) {
          List<Participant> teamParticipants = copyParticipants.sublist(
              i,
              i + teamSize > copyParticipants.length
                  ? copyParticipants.length
                  : i + teamSize);
          String teamName =
              i < teamNames.length ? teamNames[i] : "Équipe ${i + 1}";
          teams.add(Equipe(nom: teamName, participants: teamParticipants));
        }
        return teams;
      } else if (mode == 'Genre') {
        // Diviser les participants en deux groupes : hommes et femmes
        List<Participant> hommes = [];
        List<Participant> femmes = [];
        for (var participant in copyParticipants) {
          if (participant.gender == "Homme") {
            hommes.add(participant);
          } else if (participant.gender == "Femme") {
            femmes.add(participant);
          }
        }

        // Mélanger les deux groupes de manière aléatoire
        hommes.shuffle();
        femmes.shuffle();

        // Créer les équipes
        for (int i = 0; i < teamNames.length; i++) {
          teams.add(
              Equipe(nom: teamNames[i % teamNames.length], participants: []));
        }

        // Répartir les hommes et les femmes dans les équipes en alternant entre les deux
        int teamIndex = 0;
        while (hommes.isNotEmpty || femmes.isNotEmpty) {
          for (int i = 0; i < teamNames.length && hommes.isNotEmpty; i++) {
            teams[teamIndex % teamNames.length]
                .participants
                .add(hommes.removeLast());
            teamIndex++;
          }
          for (int i = 0; i < teamNames.length && femmes.isNotEmpty; i++) {
            teams[teamIndex % teamNames.length]
                .participants
                .add(femmes.removeLast());
            teamIndex++;
          }
        }

        return teams;
      } else if (mode == 'Niveau') {
        copyParticipants.sort((a, b) => (b.level ?? 0).compareTo(a.level ?? 0));

        for (int i = 0; i < teamNames.length; i++) {
          teams.add(
              Equipe(nom: teamNames[i % teamNames.length], participants: []));
        }

        int teamIndex = 0;
        for (int i = 0; i < copyParticipants.length; i++) {
          teams[teamIndex % teamNames.length]
              .participants
              .add(copyParticipants[i]);
          teamIndex++;
        }

        return teams;
      }
    }
    return teams;
  }
}
