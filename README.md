# Sort App

Sort App est une application Flutter permettant de générer des équipes pour des activités sportives, des jeux ou d'autres événements où la formation d'équipes est nécessaire.

## Fonctionnalités

- Ajouter des participants avec des attributs personnalisables tels que le nom, le genre et le niveau.
- Générer des équipes en fonction des préférences de l'utilisateur : normal, par genre ou par niveau.
- Afficher les participants ajoutés dans une interface conviviale.
- Visualiser les équipes générées avec la possibilité de faire défiler les différentes équipes.
- Personnaliser l'application avec un thème sombre et une interface utilisateur intuitive.

## Installation

1. Clonez ce dépôt sur votre machine locale :

`git clone https://gitlab.com/benzwd/flutterapp`

2. Assurez-vous d'avoir installé Flutter sur votre machine. Pour plus d'informations, consultez la [documentation Flutter](https://flutter.dev/docs/get-started/install).

3. Exécutez l'application sur un émulateur ou un appareil réel :

`flutter run`

## Captures d'écran

![Capture d'écran 1](screenshots/screenshot1.png)
![Capture d'écran 2](screenshots/screenshot2.png)
![Capture d'écran 3](screenshots/screenshot3.png)

## Structure du Projet

- `lib/`: Contient le code source de l'application.
- `main.dart`: Point d'entrée de l'application.
- `participant.dart`: Définition de la classe Participant.
- `team.dart`: Définition de la classe Equipe.
- `drawer.dart`: Widget pour les éléments du menu latéral.
- `utils.dart`: Méthodes utilitaires.

## Amelioration

- Ajout de la possibilité de sauvegarder les tirages.
- Ajout de la possibilité de modifier les participants.
- Ajout de la possibilité de modifier le nom des équipes.

## Contribution

Les contributions sont les bienvenues ! Si vous avez des idées d'amélioration, des fonctionnalités supplémentaires à ajouter ou des bogues à signaler, n'hésitez pas à ouvrir une issue ou à soumettre une pull request.

## Licence

Ce projet est sous licence [MIT](LICENSE).
